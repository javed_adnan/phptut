<?php include 'action1.php'; ?>
<?php
echo "<pre>";
print_r($_SESSION['responseclass']);
print_r($_SESSION['responsesuccess']);
echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>

    <div class="col-12 row-centered" style="margin-top:20px;">
        <p>CRUD</p>
    </div>
    <?php if (isset($_SESSION['responsesuccess'])) { ?>
        <div class="alert<?php echo $_SESSION['responseclass']; ?>">
            <?php echo $_SESSION['responsesuccess']; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php unset($_SESSION['responseclass']) ?>
    <?php } ?>
    <div class="row">
        <div class="col-3 ml-10" style="margin-left:15px;">
            <form action="action1.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="fullname" id="" class="form-control"></div>
                <div class="form-group">
                    <input type="email" name="email" id="" class="form-control">
                </div>
                <div class="form-group">
                    <input type="tel" name="phone" id="" class="form-control">
                </div>
                <div class="form-group">
                    <input type="file" name="photo" id="" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Submit" class="btn btn-danger">
                </div>
            </form>
        </div>
        <div class="col-8">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Photo</th>
                        <th colspan="3"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="3"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>




    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>